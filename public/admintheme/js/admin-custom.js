$(document).ready(function () {

    $(".summernote").summernote({
//            ['insert', ['link', 'picture', 'video']],
        onCreateLink : function(originalLink) {
            return originalLink; // return original link
        },
        pasteHTML: '<b>inserted html</b> ',
        height: 300,
        lang: 'ru-RU',
        dialogsFade: true,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'video', 'picture']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['height', ['height']]
        ],
        callbacks: {
            // Clear all formatting of the pasted text
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                setTimeout( function(){
                    document.execCommand( 'insertText', false, bufferText );
                }, 10 );
            },
            onImageUpload: function (image) {
                sendFile(image[0]);
            }
            // onMediaDelete : function(target) {
            //     // alert(target[0].src);
            //     deleteFile(target[0].src);
            // }


        }
    });



    function sendFile(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: "POST",
            url: '/uploaded',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(url) {
                console.log(url);
                $('.summernote').summernote("insertImage", url);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }






    $(".summernote-mini").summernote({
//            ['insert', ['link', 'picture', 'video']],
        onCreateLink : function(originalLink) {
            return originalLink; // return original link
        },
        pasteHTML: '<b>inserted html</b> ',
        height: 150,
        lang: 'ru-RU',
        dialogsFade: true,
        toolbar: [
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
                ['view', ['fullscreen', 'codeview']]

        ],

        callbacks: {
            // Clear all formatting of the pasted text
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                setTimeout( function(){
                    document.execCommand( 'insertText', false, bufferText );
                }, 10 );
            }
        }
    });

    $('#create_url').keyup(function () {
        translit($(this).val().toLowerCase(), $('#url_insert'));
    });

    $('#weight').inputmask('numeric', '[99]').css('text-align', 'left');







    function translit(source, $obj) {
        // Символ, на который будут заменяться все спецсимволы
        var space = '-';
        // Берем значение из нужного поля и переводим в нижний регистр
        var text = source;
        //var text = document.getElementById('name').value.toLowerCase();
        // Массив для транслитерации
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': space, 'ы': 'y',
            'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
            ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space, '#': space, '$': space,
            '%': space, '^': space, '&': space, '*': space, '(': space, ')': space, '-': space, '\=': space,
            '+': space, '[': space, ']': space, '\\': space, '|': space, '/': space, '.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space, '?': space, '<': space,
            '>': space, '№': space
        }

        var result = '';

        var curent_sim = '';

        for (i = 0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if (transl[text[i]] != undefined) {
                if (curent_sim != transl[text[i]] || curent_sim != space) {
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }

        result = TrimStr(result);

        // Выводим результат
        $obj.val(result);
        //document.getElementById('alias').value = result;
    }

    function TrimStr(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }



});
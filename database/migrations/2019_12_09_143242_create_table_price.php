<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrice extends Migration
{
    public function up()
    {
        Schema::create('price', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comments('Название услуги');
            $table->string('description')->comments('Описание к услуге');
            $table->string('one_price')->comments('Цена 1');
            $table->string('two_price')->comments('Цена 2');
            $table->string('three_price')->comments('Цена 3');
            $table->string('four_price')->comments('Цена 4');
            $table->tinyInteger('category_id')->comments('Категория');
            $table->tinyInteger('type_id')->comments('Тип представления');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('price');
    }
}

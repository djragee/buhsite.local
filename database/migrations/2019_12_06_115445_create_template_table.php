<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTable extends Migration
{
    public function up()
    {
        Schema::create('template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_one')->nullable()->comments('Телефон 1');
            $table->string('phone_two')->nullable()->comments('Телефон 2');
            $table->string('address')->nullable()->comments('Адрес компании');
            $table->string('company_name')->nullable()->comments('Наздвание компании');
            $table->string('time_work')->nullable()->comments('Время работы');
            $table->string('time_work_string')->nullable()->comments('Доп поле для отображения на сайте');
            $table->string('mail')->nullable()->comments('почта для отображения на сайте');
            $table->string('mail_callback')->nullable()->comments('почта для обратной связи');
            // Соц. сети
            $table->string('fb')->nullable()->comments('Фейсбук');
            $table->string('inst')->nullable()->comments('Инстаграм');
            $table->string('vk')->nullable()->comments('Вконтакте');
            // карта
            $table->string('latitude')->nullable()->comments('ширина');
            $table->string('landitude')->nullable()->comments('долгота');
            // SEO
            $table->text('counters_header')->nullable()->comments('счетчики для хедера');
            $table->text('counters_footer')->nullable()->comments('счетчики для подвала');
            $table->string('title_main')->nullable()->comments('Заголовок для главной страницы');
            $table->text('description_main')->nullable()->comments('Описание для главной страницы');
            $table->string('keywords_main')->nullable()->comments('Ключевики для главной страницы');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('template');
    }
}

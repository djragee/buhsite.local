<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'template';

    protected $fillable = [
        'phone_one', 'phone_two', 'address', 'company_name', 'time_work',
        'time_work_string', 'mail', 'mail_callback', 'fb', 'inst',
        'vk', 'latitude', 'landitude', 'counters_header', 'counters_footer' , 'title_main', 'description_main', 'keywords_main'
    ];
    public function getPhoneOneClearAttribute() {
        return preg_replace('/[^0-9]/', '', $this->phone_one);
    }
    public function getPhoneTwoClearAttribute() {
        return preg_replace('/[^0-9]/', '', $this->phone_two);
    }
}

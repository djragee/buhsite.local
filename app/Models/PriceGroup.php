<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceGroup extends Model
{
    protected $table = 'price_group';

    public function getPrice() {
        return $this->hasMany('App\Models\Price', 'category_id', 'id');
    }

}


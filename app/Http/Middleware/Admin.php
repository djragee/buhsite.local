<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Admin {

    /**
     * только для авторизованных пользователей с roles 1
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::check()) {
            return $next($request);
        } else {
            Auth::logout();
            return redirect()->route('login');
        }
    }

}
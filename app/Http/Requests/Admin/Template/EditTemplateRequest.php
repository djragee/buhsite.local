<?php

namespace App\Http\Requests\Admin\Template;

use Illuminate\Foundation\Http\FormRequest;

class EditTemplateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_one' => 'required|min:2|max:25',
            'phone_two' => 'required|min:2|max:25',
            'address' => 'required|min:2|max:200',
            'company_name' => 'required|min:2|max:200',
            'time_work' => 'required|min:2|max:200',
            'time_work_string' => 'max:190',
            'mail' => 'required|min:2|max:190',
            'mail_callback' => 'required|min:2|max:190',
            'fb' => 'min:1|max:150',
            'inst' => 'min:1|max:150',
            'vk' => 'min:1|max:150',
            'latitude' => 'required|min:2|max:25',
            'landitude' => 'required|min:2|max:25',
            'title_main' => 'min:2|max:200',
            'description_main' => 'min:2',
            'keywords_main' => 'min:2|max:200',
        ];
    }

    public function attributes()
    {
        return [
            'phone_one' => 'Телефон 1',
            'phone_two' => 'Телефон 2',
            'address' => 'Адрес',
            'company_name' => 'Название компании',
            'time_work' => 'Время работы',
            'time_work_string' => 'Приписка ко времени',
            'mail' => 'Почта на сайте',
            'mail_callback' => 'Почта для обратной связи',
            'fb' => 'Фейсбук',
            'inst' => 'Инстаграм',
            'vk' => 'Вконтакте',
            'latitude' => 'Широта',
            'landitude' => 'Долгота',
            'title_main' => 'Заголовок главной страницы',
            'description_main' => 'Описание главной страницы',
            'keywords_main' => 'Ключевики главной страницы',
        ];
    }
}

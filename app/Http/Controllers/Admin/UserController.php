<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        $Users = \App\User::get();
        $data = [
            'title' => 'Список пользователей',
            'layout' => 'admin/users/index',
            'list' => $Users,
        ];
        return view('template.admin.template', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Добавление пользователя',
            'layout' => 'admin/users/create',
        ];
        return view('template.admin.template', $data);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|max:100',
            'email' => 'required|email|unique:users,email|max:100',
            'password' => ['required',
                'min:6',
                'max:15'
            ]
        ];

        $niceNames = [
            'name' => 'имя',
            'email' => 'почта',
            'password' => 'пароль'
        ];
        $this->validate($request, $rules, [], $niceNames);
        //сохранение формы нового пользователя
        $User = new \App\User;
        $User->name = $request->input('name');
        $User->email = $request->input('email');
        $User->password = bcrypt($request->input('password'));
        $User->save();
        return redirect(route('admin.users.index'));
    }


    public function edit($id)
    {
        $User = \App\User::whereId($id)->first();
        if (!$User) {
            abort(404);
        }
        $data = [
            'title' => 'Редактирование пользователя',
            'layout' => 'admin/users/edit',
            'user' => $User,
        ];
        return view('template.admin.template', $data);
    }

    public function update(Request $request, $id)
    {
        if ($request->input('password')) {
            $rules = [
                'name' => 'required|min:2|max:100',
                'email' => 'required|email|max:100',
                'password' => ['required',
                    'min:6',
                    'max:15',
                    'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/'
                ]
            ];
        } else {
            $rules = [
                'name' => 'required|min:2|max:100',
                'email' => 'required|email|max:100',
            ];
        }

        $niceNames = [
            'name' => 'имя',
            'email' => 'почта',
            'password' => 'пароль'
        ];
        $this->validate($request, $rules, [], $niceNames);
        //сохранение формы нового пользователя
        $User = \App\User::whereId($id)->first();

        $User->name = $request->input('name');
        $User->email = $request->input('email');
        if ($request->input('password')) {
            $User->password = bcrypt($request->input('password'));
        }
        $User->save();
        return redirect(route('admin.users.index'));
    }


    public function destroy($id)
    {
        $Model = \App\User::whereId($id)->first();
        $Model->delete();
        return redirect(route('admin.users.index'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Template\EditTemplateRequest;
use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TemplateController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        $Model = Template::first();
        if (!$Model) {
            $Model = new Template();
        }
        $data = [
            'title' => 'Шаблон сайта',
            'layout' => 'admin/template/index',
            'list' => $Model,
        ];
        return view('template.admin.template', $data);
    }


    public function store(EditTemplateRequest $request)
    {
        $Model = Template::first();
        if (!$Model) {
            $Model = new Template();
        }
        $Model->phone_one = $request->input('phone_one', null);
        $Model->phone_two = $request->input('phone_two', null);
        $Model->address = $request->input('address', null);
        $Model->company_name = $request->input('company_name', null);
        $Model->time_work = $request->input('time_work', null);
        $Model->time_work_string = $request->input('time_work_string', null);
        $Model->mail = $request->input('mail', null);
        $Model->mail_callback = $request->input('mail_callback', null);
        $Model->fb = $request->input('fb', null);
        $Model->inst = $request->input('inst', null);
        $Model->vk = $request->input('vk', null);
        $Model->latitude = $request->input('latitude', null);
        $Model->landitude = $request->input('landitude', null);
        $Model->counters_header = $request->input('counters_header', null);
        $Model->counters_footer = $request->input('counters_footer', null);
        $Model->title_main = $request->input('title_main', null);
        $Model->description_main = $request->input('description_main', null);
        $Model->keywords_main = $request->input('keywords_main', null);
        $Model->save();
        return redirect()->back();
    }

}

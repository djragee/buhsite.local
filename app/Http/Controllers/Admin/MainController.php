<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function __construct() {
        $data = [
            'user' => Auth::user(),
        ];
        view()->share($data);
    }

    public function index() {
        $data = [
            'layout' => 'admin/main/index',
            'title' => 'Админ панель',
        ];

        return view('template.admin.template', $data);
    }
}

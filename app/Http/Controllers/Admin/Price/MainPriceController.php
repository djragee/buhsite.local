<?php

namespace App\Http\Controllers\Admin\Price;

use App\Models\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainPriceController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        $Model = Price::where('type_id', '=', 1)->get();
        $data = [
            'layout' => 'admin/price/index',
            'title' => 'Основной прайс',
            'list' => $Model
        ];
        return view('template.admin.template', $data);
    }


    public function store(Request $request)
    {
        if ($request->input('keys')) {
            foreach ($request->input('keys') as $key => $value) {
                if (isset($value['id']) && !empty($value['id'])) {
                    $Serv = Price::whereId($value['id'])->first();
                    if ($Serv) {
                        $Serv->name = $value['name'];
                        $Serv->save();
                    }
                } else {
                    $Serv = new Price;
                    $Serv->name = $value['name'];
                    $Serv->description = $value['name'];
                    $Serv->one_price = 0;
                    $Serv->two_price = 0;
                    $Serv->three_price = 0;
                    $Serv->four_price = 0;
                    $Serv->category_id = 0;
                    $Serv->type_id = 1;

                    $Serv->save();
                }
            }
        }

        return redirect(route('admin.mainprice.index'));
    }

    public function del_price_item(Request $request)
    {
        Price::whereId($request->input('id'))->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

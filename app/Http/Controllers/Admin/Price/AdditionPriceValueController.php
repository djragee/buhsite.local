<?php

namespace App\Http\Controllers\Admin\Price;

use App\Models\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdditionPriceValueController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('item_value')) {
            foreach ($request->input('item_value') as $key => $value) {

                $Model = Price::whereId($value['group_items_id'])->first();

                if ($Model) {
                    $Model->one_price = $value['one_price'];
                    $Model->category_id = $value['category_id'];
                    $Model->two_price = 0;
                    $Model->three_price = 0;
                    $Model->four_price = 0;
                    $Model->save();
                }
            }
        }
        return redirect(route('admin.additionprice.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
<?php

namespace App\Http\Controllers\Admin\Price;

use App\Models\Price;
use App\Models\PriceGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdditionPriceGroupController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        $Model = PriceGroup::get();
        $data = [
            'layout' => 'admin/price/category',
            'title' => 'Категории для дополнительного прайса',
            'list' => $Model
        ];
        return view('template.admin.template', $data);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        $rules = [
            'name' => 'required',
        ];
        $niceNames = [
            'name' => 'название',
        ];
        $this->validate($request, $rules, [], $niceNames);

        $Model = new PriceGroup();
        $Model->name = $request->name;
        $Model->save();

        return redirect()->route('admin.pricegroup.index');
    }



    public function update(Request $request, $id)
    {
        {
            $rules = [
                'name' => 'required',
            ];
            $niceNames = [
                'name' => 'название',
            ];
            $this->validate($request, $rules, [], $niceNames);
            $Model = PriceGroup::findOrFail($id);
            $Model->name = $request->name;
            $Model->save();

            return redirect()->route('admin.pricegroup.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = PriceGroup::findOrFail($id);
        $price = Price::where('category_id', '=', $group->id)->get();

        if ($price) {
            foreach ($price as $item) {
                $item->category_id = 0;
                $item->save();
            }
        }
        $group->delete();
        return redirect(route('admin.pricegroup.index'));
    }
}

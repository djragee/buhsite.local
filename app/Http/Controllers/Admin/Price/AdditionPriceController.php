<?php

namespace App\Http\Controllers\Admin\Price;

use App\Models\Price;
use App\Models\PriceGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdditionPriceController extends Controller
{
    public function __construct()
    {
        $data = [
            'user' => Auth::user()
        ];
        view()->share($data);
    }

    public function index()
    {
        $Model = Price::where('type_id', '=', 2)->get();
        $Model_Group = PriceGroup::get();
        $data = [
            'layout' => 'admin/price/addition',
            'title' => 'Дополнительный прайс',
            'list' => $Model,
            'list_group' => $Model_Group,
        ];
        return view('template.admin.template', $data);
    }


    public function store(Request $request)
    {
        if ($request->input('keys')) {
            foreach ($request->input('keys') as $key => $value) {
                if (isset($value['id']) && !empty($value['id'])) {
                    $Serv = Price::whereId($value['id'])->first();
                    if ($Serv) {
                        $Serv->name = $value['name'];
                        $Serv->save();
                    }
                } else {
                    $Serv = new Price;
                    $Serv->name = $value['name'];
                    $Serv->description = $value['name'];
                    $Serv->one_price = 0;
                    $Serv->two_price = 0;
                    $Serv->three_price = 0;
                    $Serv->four_price = 0;
                    $Serv->category_id = 0;
                    $Serv->type_id = 2;

                    $Serv->save();
                }
            }
        }

        return redirect(route('admin.additionprice.index'));
    }

    public function del_price_item(Request $request)
    {
        Price::whereId($request->input('id'))->delete();
    }

}

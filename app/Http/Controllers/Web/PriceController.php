<?php

namespace App\Http\Controllers\Web;

use App\Models\Price;
use App\Models\PriceGroup;
use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PriceController extends Controller
{
    public function __construct() {
        $data = array(
            'template' => Template::first()
        );
        view()->share($data);
    }

    public function index(){
        $data = [
            'price_one' => Price::where('type_id', '=', 1)->get(),
            'price_two' => Price::where('type_id', '=', 2)->get(),
            'price_group' => PriceGroup::get(),
            'layout' => 'index',
            'title' => 'Прайс-лист',
            'seo_title' => 'Прайс-лист | Новосибирский дом права',
            'seo_description' => 'Бухгалтерские услуги в Новосибисрке',
            'seo_keywords' => 'бухгалтерские услуги'
        ];
        return view('public.price.template',$data);
    }
}

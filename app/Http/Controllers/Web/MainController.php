<?php

namespace App\Http\Controllers\Web;

use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function __construct() {
        $data = array(
            'template' => Template::first()
        );
        view()->share($data);
    }

    public function index(){
        $data = [
            'layout' => 'index',
//            'data' => mainpage::whereId(1)->firstOrFail(),
//            'dataimg' => mainlink::with('mainImg')
//                ->orderBy('weight','ASC')
//                ->get(),
        ];
        return view('public.main.template',$data);
    }
}

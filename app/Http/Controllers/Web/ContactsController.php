<?php

namespace App\Http\Controllers\Web;

use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    public function __construct() {
        $data = array(
            'template' => Template::first()
        );
        view()->share($data);
    }

    public function index(){
        $data = [
            'layout' => 'index',
            'title' => 'Контакты',
            'seo_title' => 'Контакты | Новосибирский дом права',
            'seo_description' => 'Бухгалтерские услуги в Новосибисрке',
            'seo_keywords' => 'бухгалтерские услуги'
        ];
        return view('public.contacts.template',$data);
    }

    public function contactsSendForm(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|max:20',
            'phone' => 'required',
            'subject' => 'required|min:4|max:50',
            'message' => 'required',
        ];
        $niceNames = [
            'name' => 'имя',
            'phone' => 'телефон',
            'subject' => 'тема письма',
            'message' => 'сообщение',
        ];
        $this->validate($request, $rules, [], $niceNames);

        if ($request->ajax()) {
            $data = [
                'status' => true,
                'message' => 'Ваше сообщение отправлено, спасибо!',
            ];

            $temp = Template::first();
            $to = $temp->mail_callback; //Почта получателя, через запятую можно указать сколько угодно адресов
            $subject = $request->input('subject'); //Загаловок сообщения
            $message = '
                <html>
                            <style>
                body {
                    margin: 0px;
                    padding: 0px;
                    background-color: #e3e3e3;
                    color: #333333;
                    font-family: sans-serif;
                    line-height: 22px;
                    width: 100%;
                }
            </style>
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%; min-width:500px;">
                <tbody>
                <!--шапка письма-->
                <tr>
                    <td style="">
                        <table style="width:100%; background: #e3e3e3;">
                            <tbody>
                            <tr>
                                <td style="padding: 0px 20px; color: #000000">
                                    <h2 style="font-size: 20px;">NDP.su</h2>
                                </td>
                                <td style="text-align: right; padding: 0px 20px;">
                                    <a href="http://ndp.su"
                                       style="text-decoration: none; padding: 10px 20px; color: #2E8ED7 !important; border: 2px solid #2E8ED7; background-color: #fff; border-radius: 4px; color: #2E8ED7">
                                        Сайт
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                <td style="padding: 0px 20px;">
                    <table style="width:100%;">
                        <tbody>
                        <tr>
                            <td style="background-color: #fff; color: #333333; padding: 10px;">
            
                                <b>Имя:</b> '. $request->input('name') .'<br>
                                <b>Телефон:</b> '. $request->input('phone') .'<br>
                                <b>Сообщение:</b> '. $request->input('message') .'<br>
                            </td>
            
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            
            
                </tbody>
            </table>
                    
                </html>'; //Текст нащего сообщения можно использовать HTML теги

            $headers = "Content-type: text/html; charset=utf-8 \r\n"; //Кодировка письма
            $headers .= "From: Сайт ndp.su <noreply@ndp.su>\r\n"; //Наименование и почта отправителя
            @mail($to, $subject, $message, $headers); //Отправка письма с помощью функции mail

            return response()->json($data);
        }

    }
}

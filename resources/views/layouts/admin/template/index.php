<form class="form-horizontal" role="form" method="POST" action="<?= route('admin.template.store') ?>"
      enctype="multipart/form-data" data-toggle="validator" novalidate="true">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('phone_one')) echo "has-error" ?>">
                            <label class="col-sm-12">Телефон 1</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control phone" name="phone_one"
                                       value="<?= old('phone_one', $list->phone_one); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('phone_one')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('phone_one') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('phone_two')) echo "has-error" ?>">
                            <label class="col-sm-12">Телефон 2</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control phone" name="phone_two"
                                       value="<?= old('phone_two', $list->phone_two); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('phone_two')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('phone_two') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('mail')) echo "has-error" ?>">
                            <label class="col-sm-12">Почта для сайта</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control mail" name="mail"
                                       value="<?= old('mail', $list->mail); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('mail')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('mail') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('mail_callback')) echo "has-error" ?>">
                            <label class="col-sm-12">Почта для обратной связи</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control mail" name="mail_callback"
                                       value="<?= old('mail_callback', $list->mail_callback); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('mail_callback')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('mail_callback') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('company_name')) echo "has-error" ?>">
                            <label class="col-sm-12">Название компании</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="company_name"
                                       value="<?= old('company_name', $list->company_name); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('company_name')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('company_name') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('address')) echo "has-error" ?>">
                            <label class="col-sm-12">Адрес</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="address"
                                       value="<?= old('address', $list->address); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('address')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('address') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('time_work')) echo "has-error" ?>">
                            <label class="col-sm-12">Часы работы</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="time_work"
                                       value="<?= old('time_work', $list->time_work); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('time_work')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('time_work') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group <?php if ($errors->has('time_work_string')) echo "has-error" ?>">
                            <label class="col-sm-12">Приписка к часам работы</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="time_work_string"
                                       value="<?= old('time_work_string', $list->time_work_string); ?>"
                                       placeholder="Не обязательное поле">
                                <?php if (!$errors->has('time_work_string')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('time_work_string') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('vk')) echo "has-error" ?>">
                            <label class="col-sm-12">Vkontakte</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="vk"
                                       value="<?= old('vk', $list->vk); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('vk')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('vk') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('fb')) echo "has-error" ?>">
                            <label class="col-sm-12">Facebook</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="fb"
                                       value="<?= old('fb', $list->fb); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('fb')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('fb') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('inst')) echo "has-error" ?>">
                            <label class="col-sm-12">Instagram</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="inst"
                                       value="<?= old('inst', $list->inst); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('inst')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('inst') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-12">Код для вывода счетчиков HEAD</label>
                            <div class="col-sm-12">
                                <textarea name="counters_header" class="form-control" rows="3"
                                          placeholder="Вставлять код для счетчиков"><?= old('counters_header', $list->counters_header); ?></textarea>
                                <?php if (!$errors->has('counters_header')) { ?>
                                    <span class="help-block"><small>Не обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('counters_header') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-12">Код для вывода счетчиков FOOTER</label>
                            <div class="col-sm-12">
                                <textarea name="counters_footer" class="form-control" rows="3"
                                          placeholder="Вставлять код для счетчиков"><?= old('counters_footer', $list->counters_footer); ?></textarea>
                                <?php if (!$errors->has('counters_footer')) { ?>
                                    <span class="help-block"><small>Не обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('counters_footer') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('title_main')) echo "has-error" ?>">
                            <label class="col-sm-12">Title главной страницы сайта</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="title_main"
                                       value="<?= old('title_main', $list->title_main); ?>"
                                       placeholder="Обязательное поле" required>
                                <?php if (!$errors->has('title_main')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('title_main') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="form-group <?php if ($errors->has('description_main')) echo 'has-error' ?>">
                            <label for="description_main" class="control-label col-md-12">Description главной страницы
                                сайта</label>
                            <div class="col-md-12">
                            <textarea rows="3" id="main_text" class="form-control"
                                      placeholder="Введите Description главной страницы
                                сайта" name="description_main"
                                      style="min-height: 70px;"><?php if ($errors->has('description_main') || count($errors)) {
                                    echo old('description_main');
                                } else {
                                    echo $list->description_main;
                                } ?></textarea>
                                <?php if ($errors->has('description_main')) { ?>
                                    <span class="help-block">
                                    <?php echo $errors->first('description_main'); ?>
                                </span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                    <div class="form-group <?php if ($errors->has('keywords_main')) echo "has-error" ?>">
                        <label class="col-sm-12">Keywords главной страницы сайта</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="keywords_main"
                                   value="<?= old('keywords_main', $list->keywords_main); ?>"
                                   placeholder="Обязательное поле" required>
                            <?php if (!$errors->has('keywords_main')) { ?>
                                <span class="help-block"><small>Обязательное поле</small></span>
                            <?php } else { ?>
                                <span class="help-block"><small><?= $errors->first('keywords_main') ?></small></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                    <div class="col-md-12">
                        <h4>Расположение на карте</h4>
                        <div class="panel-body">
                            <div class="col-md-5">
                                <div class="form-group ">
                                    <label>lat (широта) <span class="red">*</span></label>
                                    <input type="text" id="office-address_lat" placeholder="широта"
                                           name="latitude"
                                           class="form-control"
                                           value="<?php if ($errors->has('latitude') || count($errors)) {
                                               echo old('latitude');
                                           } else {
                                               echo $list->latitude;
                                           } ?>">
                                    <?php if ($errors->has('latitude')) { ?>
                                        <span class="help-block"><small
                                                    style="color: red"><?= $errors->first('latitude') ?></small>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group ">
                                    <label>lan (долгота) <span class="red">*</span></label>
                                    <input type="text" id="office-address_lng" placeholder="долгота"
                                           name="landitude"
                                           class="form-control"
                                           value="<?php if ($errors->has('landitude') || count($errors)) {
                                               echo old('landitude');
                                           } else {
                                               echo $list->landitude;
                                           } ?>">
                                    <?php if ($errors->has('landitude')) { ?>
                                        <span class="help-block"><small
                                                    style="color: red"><?= $errors->first('landitude') ?></small>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <!-- Button trigger modal -->
                                <a class="btn btn-info" data-toggle="modal" data-target="#popup-map"
                                   style="margin-top: 27px"><i
                                            class="fa fa-location-arrow "></i> Указать</a>

                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    </div>

    <div class="form-group">
        <?= csrf_field() ?>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить</button>
            </div>
        </div>
    </div>
</form>

<div id="popup-map" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Выберите координаты офиса</h4></div>
            <div class="modal-body">
                <div id="map" style="width: 100%; height: 400px"></div>
                <script type="text/javascript">
                    ymaps.ready(init);

                    function init() {
                        var myMap = new ymaps.Map("map", {
                                center: [55.025021, 82.929525],
                                zoom: 11
                            }),

                            myPlacemark = new ymaps.Placemark([55.025021, 82.929525], {
                                hintContent: 'Переместите метку для получения координат!'
                            }, {
                                draggable: true
                            });

                        myPlacemark.events.add('dragend', function (e) {
                            var coords = e.get('target').geometry.getCoordinates();
                            $('#office-address_lat').val(coords[0].toPrecision(10));
                            $('#office-address_lng').val(coords[1].toPrecision(10));
                        });
                        myMap.geoObjects.add(myPlacemark);
                    }
                </script>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(document).ready(function () {
        $('.mail').inputmask('email');
        $('.phone').inputmask({"mask": "+7 (999) 999-99-99"});
    });


</script>


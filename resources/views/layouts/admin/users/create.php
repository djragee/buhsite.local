
    <div class="container-fluid">
        <!-- breadcrumbs-->
        <div class="row bg-title">
            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Добавление нового пользователя</h4>
            </div>

            <div class="col-sm-16">
                <ul class="side-icon-text pull-right">
                    <li><a href="<?= route('admin.users.index') ?>"><span class="circle circle-sm bg-success di"><i class="ti-list-ol"></i></span><span>Перейти к с списку</span></a></li>
                </ul>
            </div>

        </div>
        <!-- breadcrumbs-->
        <!-- .row -->
        <div class="row animated fadeInLeft">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal" action="<?= route('admin.users.store') ?>" method="post">

                        <div class="form-group <?php if ($errors->has('name')) echo "has-error" ?>">
                            <label class="col-sm-12">Имя</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="name"
                                       value="<?php if ($errors->has('name')) {
                                           echo old('name');
                                       }?>"
                                       placeholder="Введите ваше имя" required>
                                <?php if (!$errors->has('name')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('name') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group <?php if ($errors->has('email')) echo "has-error" ?>">
                            <label class="col-sm-12">Email</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="email"
                                       value="<?php if ($errors->has('email')) {
                                           echo old('email');
                                       }?>"
                                       placeholder="Введите email" >
                                <?php if (!$errors->has('email')) { ?>

                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('email') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group <?php if ($errors->has('password')) echo "has-error" ?>">
                            <label class="col-sm-12">Новый пароль</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Введите новый пароль" autocomplete="new-password">
                                <?php if (!$errors->has('password')) { ?>

                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('password') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <?= csrf_field() ?>
                        </div>
                        <button type="submit" class="btn btn-info btn-outline waves-effect waves-light m-r-10">Сохранить
                            изменения
                        </button>
                        <a href="<?= route('admin.users.index') ?>">
                            <button type="button" class="btn btn-success btn-outline waves-effect waves-light">Назад
                            </button>
                        </a>

                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>

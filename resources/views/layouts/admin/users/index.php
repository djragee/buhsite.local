
    <div class="container-fluid">
        <!-- breadcrumbs-->
        <div class="row bg-title">
            <div class="col-sm-12">
                <ul class="side-icon-text pull-right">
                    <li><a href="<?= route('admin.users.create') ?>"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Добавить пользователя</span></a></li>
                </ul>
            </div>
        </div>
        <!-- breadcrumbs-->
        <!-- .row -->
        <div class="row animated fadeInLeft">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="ibox float-e-margins">

                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Email</th>
                                        <th>Регистрация</th>
                                        <th>Редактировать</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list as $key => $value) {
                                        ?>
                                        <tr class="gradeC">
                                            <td><?php echo $value->name ?></td>
                                            <td><?php echo $value->email ?></td>
                                            <td><?php echo $value->created_at ?></td>
                                            <td style="display: flex">
                                                <a href="<?= route('admin.users.edit', $value)?>" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" title="Редактировать"><i class="ti-pencil-alt"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function () {
        $('.dataTables-example').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json"
            },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
    });
</script>
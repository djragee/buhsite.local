
    <div class="container-fluid">
        <!-- breadcrumbs-->
        <div class="row bg-title">
            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                <h4 class="page-title">Редактирование пользователя "<?= $user->name ?>"</h4></div>
            <div class="col-lg-6 col-sm-6 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li class="active"><a href="<?= route('admin.users.index') ?>">Список пользователей</a></li>
                </ol>
            </div>
        </div>
        <!-- breadcrumbs-->
        <!-- .row -->
        <div class="row animated fadeInLeft">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Настройки пользователя</h3>
                    <p class="text-muted m-b-30 font-13">Вы можете отредактировать данные пользователя</p>
                    <form class="form-horizontal" action="<?= route('admin.users.update', $user->id) ?>" method="post">

                        <div class="form-group <?php if ($errors->has('name')) echo "has-error" ?>">
                            <label class="col-sm-12">Имя</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="name"
                                       value="<?php if ($errors->has('name')) {
                                           echo old('name');
                                       } else {
                                           echo $user->name;
                                       } ?>"
                                       placeholder="Введите ваше имя" required>
                                <?php if (!$errors->has('name')) { ?>
                                    <span class="help-block"><small>Обязательное поле</small></span>
                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('name') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group <?php if ($errors->has('email')) echo "has-error" ?>">
                            <label class="col-sm-12">Email</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="email"
                                       value="<?php if ($errors->has('email')) {
                                           echo old('email');
                                       } else {
                                           echo $user->email;
                                       } ?>"
                                       placeholder="Введите email" >
                                <?php if (!$errors->has('email')) { ?>

                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('email') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group <?php if ($errors->has('password')) echo "has-error" ?>">
                            <label class="col-sm-12">Новый пароль</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Введите новый пароль" autocomplete="new-password">
                                <?php if (!$errors->has('password')) { ?>

                                <?php } else { ?>
                                    <span class="help-block"><small><?= $errors->first('password') ?></small></span>
                                <?php } ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <?= csrf_field() ?>
                            <?= method_field('put') ?>
                        </div>
                        <button type="submit" class="btn btn-info btn-outline waves-effect waves-light m-r-10">Сохранить
                            изменения
                        </button>
                        <a href="<?= route('admin.users.index') ?>">
                            <button type="button" class="btn btn-success btn-outline waves-effect waves-light">Назад
                            </button>
                        </a>

                    </form>
                </div>

                <div class="row animated fadeInLeft">
                    <div class="col-sm-12 mt20">
                        <div class="white-box">
                            <h4>Удалить пользователя?</h4>
                            <form class="form-horizontal" action="<?= route('admin.users.destroy', $user->id) ?>"
                                  method="post">
                                <div class="form-group">
                                    <?= csrf_field() ?>
                                    <?= method_field('delete') ?>
                                </div>
                                <button type="submit" onclick="return confirm('Вы хотите удалить этого пользователяь?')" class="btn btn-danger btn-outline waves-effect waves-light m-r-10">Да,
                                    удалить
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->

    </div>

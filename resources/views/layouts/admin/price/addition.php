<form class="form-horizontal" role="form" method="POST" action="<?= route('admin.additionprice.store') ?>"
      enctype="multipart/form-data" data-toggle="validator" novalidate="true">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">


                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('weight')) echo "has-error" ?>">
                            <div class="col-md-12">
                                <div class="service_keys-ctr ">
                                    <div class="service-keys-ctr form-group">
                                        <label style="margin-bottom: 0;">Услуги категории</label>
                                        <div class="service-keys-header">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="col_offset">#</td>
                                                    <td class="col_in_table"><i class="fa fa-table"></i></td>
                                                    <td>Название</td>
                                                    <td class="col_ctrl"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="service-keys-items ui-sortable">
                                            <?php foreach ($list as $key => $value) { ?>
                                                <div class="item">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td class="col_offset"><?= $key ?></td>
                                                            <td class="col_in_table">
                                                                <input type="hidden" class="form-control"
                                                                       name="keys[<?= $key ?>][in_table]"
                                                                       value="<?= $value->in_table ?>">
                                                                <div class="in_table-checker <?php if ($value->in_table == 1) echo 'checked'; ?>"></div>
                                                            </td>
                                                            <td class="col_name">
                                                                <input type="text" class="form-control"
                                                                       name="keys[<?= $key ?>][name]" required=""
                                                                       value="<?= $value->name ?>">
                                                                <input type="hidden" class="form-control"
                                                                       name="keys[<?= $key ?>][id]"
                                                                       value="<?= $value->id ?>">
                                                                <input type="hidden"
                                                                       class="form-control js-input-offset"
                                                                       name="keys[<?= $key ?>][offset]"
                                                                       value="<?= $key ?>">

                                                            </td>
                                                            <td class="col_ctrl"><span
                                                                        class="btn btn-default js-delete-key-handler"
                                                                        data-id="<?= $value->id ?>"><i
                                                                            class="fa fa-times"></i></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <span class="js-add-key-handler"><i class="fa fa-plus"
                                                                            style="margin-right: 5px;"></i>Добавить</span>
                                    </div>

                                    <style>
                                        .service-keys-ctr {

                                        }

                                        .service-keys-header {
                                            font-weight: 700;
                                        }

                                        .service-keys-ctr table {
                                            width: 100%;
                                        }

                                        .service-keys-ctr table td {
                                            border: 1px #E5E6E7 solid;
                                            padding: 5px;
                                        }

                                        .service-keys-header table td {
                                            padding: 10px 5px;
                                        }

                                        .service-keys-ctr .col_offset {
                                            width: 50px;
                                            text-align: center;
                                        }

                                        .service-keys-ctr .col_in_table {
                                            width: 50px;
                                            text-align: center;
                                        }

                                        .service-keys-ctr .col_ctrl {
                                            width: 50px;
                                        }

                                        .service-keys-items {
                                            position: relative;
                                        }

                                        .service-keys-items .item {
                                            background: rgba(255, 255, 255, 0.5);
                                        }

                                        .service-keys-items .item .col_offset {
                                            cursor: move;
                                        }

                                        .service-keys-items .item td {
                                            border-top: 0;
                                        }

                                        .service-keys-ctr .js-add-key-handler {
                                            display: block;
                                            border: 1px #E5E6E7 solid;
                                            border-top: 0;
                                            cursor: pointer;
                                            text-align: center;
                                            padding: 10px;
                                            font-weight: 700;
                                        }

                                        .service-keys-ctr .js-add-key-handler:hover {
                                            background: #e6e6e6;
                                        }

                                        .in_table-checker {
                                            display: inline-block;
                                            width: 32px;
                                            height: 32px;
                                            border: 1px #E5E6E7 solid;
                                            font: normal normal normal 14px/1 FontAwesome;
                                            color: #E5E6E7;
                                            cursor: pointer;
                                        }

                                        .in_table-checker:before {
                                            content: "\f00c";
                                            display: inline-block;
                                            line-height: 30px;
                                        }

                                        .in_table-checker.checked {
                                            color: #fff;
                                            background: #2C8F7B;
                                            border-color: #2C8F7B;
                                        }
                                    </style>

                                    <script>
                                        $(document).ready(function () {

                                            function uniqid(prefix, more_entropy) {


                                                if (typeof prefix === 'undefined') {
                                                    prefix = '';
                                                }

                                                var retId;
                                                var formatSeed = function (seed, reqWidth) {
                                                    seed = parseInt(seed, 10)
                                                        .toString(16); // to hex str
                                                    if (reqWidth < seed.length) {
                                                        // so long we split
                                                        return seed.slice(seed.length - reqWidth);
                                                    }
                                                    if (reqWidth > seed.length) {
                                                        // so short we pad
                                                        return Array(1 + (reqWidth - seed.length))
                                                                .join('0') + seed;
                                                    }
                                                    return seed;
                                                };
                                                // BEGIN REDUNDANT
                                                if (!this.php_js) {
                                                    this.php_js = {};
                                                }
// END REDUNDANT
                                                if (!this.php_js.uniqidSeed) {
// init seed with big random int
                                                    this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
                                                }
                                                this.php_js.uniqidSeed++;
                                                // start with prefix, add current milliseconds hex string
                                                retId = prefix;
                                                retId += formatSeed(parseInt(new Date()
                                                        .getTime() / 1000, 10), 8);
                                                // add seed hex string
                                                retId += formatSeed(this.php_js.uniqidSeed, 5);
                                                if (more_entropy) {
// for more entropy we add a float lower to 10
                                                    retId += (Math.random() * 10)
                                                        .toFixed(8)
                                                        .toString();
                                                }

                                                return retId;
                                            }

                                            function recalcOffset() {
                                                var o = 1;
                                                $('.service-keys-items .item').each(function () {
                                                    $('.col_offset', $(this)).text(o);
                                                    $('.js-input-offset', $(this)).val(o);
                                                    o++;
                                                });
                                            }

                                            recalcOffset();

                                            $('.service-keys-ctr .js-add-key-handler').off('click').on('click', function () {
                                                var $item = $('<div class="item"></div>');
                                                var uid = uniqid();
                                                $item.append('<table><tbody><tr><td class="col_offset"></td><td class="col_in_table"></td><td class="col_name"></td><td class="col_ctrl"></td></tr></tbody></table>');
                                                $('.col_in_table', $item).append('<input type="hidden" class="form-control" name="keys[' + uid + '][in_table]" value="1"><div class="in_table-checker checked"></div>');
                                                $('.col_name', $item).append('<input type="text" class="form-control" name="keys[' + uid + '][name]" required=""/>');
                                                $('.col_name', $item).append('<input type="hidden" class="form-control js-input-offset" name="keys[' + uid + '][offset]"/>');
                                                $('.col_ctrl', $item).append('<span class="btn btn-default js-delete-key-handler"><i class="fa fa-times"></i></span>');

                                                $('.service-keys-items').append($item);
                                                recalcOffset();
                                            });


                                            $(document).off('click', '.service-keys-ctr .js-delete-key-handler').on('click', '.service-keys-ctr .js-delete-key-handler', function () {

                                                var t = $(this);
                                                var data = t.attr('data-id');
                                                $.ajax({
                                                    url: "<?= route('admin.mainprice.del_ajax') ?>",
                                                    type: 'POST',
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    data: {
                                                        id: data
                                                    },
                                                    success: function (h) {
                                                        t.closest('.item').remove();
                                                        recalcOffset();
                                                    }
                                                });
                                                return false;
                                            });

//                                                $(document).off('click', '.service-keys-ctr .in_table-checker').on('click', '.service-keys-ctr .in_table-checker', function () {
//                                                    $(this).toggleClass('checked');
//                                                    $('input', $(this).closest('td')).val(1 * $(this).hasClass('checked'));
//                                                });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= csrf_field() ?>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить</button>
            </div>
        </div>
    </div>
</form>


<form class="form-horizontal" role="form" method="POST" action="<?= route('admin.additionprice_item.store') ?>"
      enctype="multipart/form-data" data-toggle="validator" novalidate="true">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-wrapper collapse in">
                <div class="panel-body">

                    <div class="col-md-12">
                        <div class="form-group <?php if ($errors->has('short_name')) echo "has-error" ?>">
                            <label class="col-sm-12">Список услуг *</label>
                            <div class="col-sm-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Категория</th>
                                        <th>Цена 1</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list as $value) { ?>
                                        <tr>
                                            <td><?= $value->name ?></td>
                                            <td>
                                                <select class="form-control" name="item_value[<?= $value->id ?>][category_id]">
                                                    <option value="0">Выберите категорию</option>
                                                    <?php foreach ($list_group as $opt) { ?>
                                                        <option value="<?= $opt->id ?>"
                                                            <?php if ($opt->id == $value->category_id) echo ' selected ' ?>
                                                        ><?= $opt->name ?></option>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden"
                                                       name="item_value[<?= $value->id ?>][group_items_id]"
                                                       value="<?= $value->id ?>">
                                            </td>
                                            <td>
                                                <input type="text" name="item_value[<?= $value->id ?>][one_price]"
                                                       value="<?php
                                                       if (isset($value->one_price)) {
                                                           echo $value->one_price;
                                                       }
                                                       ?>">
                                                <input type="hidden"
                                                       name="item_value[<?= $value->id ?>][group_items_id]"
                                                       value="<?= $value->id ?>">
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= csrf_field() ?>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить</button>
            </div>
        </div>
    </div>
</form>



<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-10">Список категорий</h3>
            <div class="table-responsive">
                <table id="pricegroupTable" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th width="140">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($list)) { ?>
                        <?php foreach ($list as $key => $value) { ?>
                            <tr>
                                <td><?php echo $value->name ?></td>
                                <td style="display: flex;">
                                    <a type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" data-toggle="modal" data-target="#edit-pricegroup-<?php echo $value->id ?>" title="Редактировать"><i class="ti-pencil-alt"></i></a>
                                    <div id="edit-pricegroup-<?php echo $value->id ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Редактирование</h4></div>
                                                <div class="modal-body">
                                                    <form role="form" method="POST" action="<?= route('admin.pricegroup.update', $value->id) ?>" enctype="multipart/form-data" data-toggle="validator" novalidate="true">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="control-label">Название:</label>
                                                            <input type="text" class="form-control" id="pricegroup-edit-name" name="name" value="<?php echo $value->name ?>" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <input type="hidden" name="_method" value="PUT">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Закрыть</button>
                                                            <button type="submit" class="btn btn-success waves-effect waves-light">Изменить</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <form method="POST" action="<?= route('admin.pricegroup.destroy', $value->id) ?>" accept-charset="UTF-8" id="deleteForm">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" onclick="return confirm('Вы хотите удалить эту запись?')" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" title="Удалить"><i class="ti-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <form class="form-horizontal" role="form" method="POST" action="<?= route('admin.pricegroup.store') ?>" enctype="multipart/form-data">
            <div class="panel panel-default">
                <div class="panel-heading"><b><i class="ti-save"></i></b> Добавление новой категории</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-group <?php if ($errors->has('name')) echo "has-error" ?>">
                            <label for="name" class="col-md-12">Название <span class="red">*</span></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Введите название">
                                <span class="help-block">
                                    <?php echo $errors->first('name'); ?>
                                </span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success m-t-20 pull-right"> Добавить</button>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    </div>

                </div>
            </div>
        </form>
    </div>

</div>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">

    <title><?= $title ?></title>

    <!-- CSS -->
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="/js/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="/admintheme/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/colors/blue-dark.css" id="theme" rel="stylesheet">
    <link href="/admintheme/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="/admintheme/css/select2.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>    <!--    скачать на сайт-->

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <link href="/css/admin-style.css" rel="stylesheet">
    <link href="/admintheme/css/admin-style.css" rel="stylesheet">

    <link rel="stylesheet" href="/admintheme/css/dropify.min.css">
    <link rel="stylesheet" href="/admintheme/css/bootstrap-treeview.min.css">

    <script src="/js/jquery/dist/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>


    <!-- CSS -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


    <![endif]-->
</head>

<body class="fix-sidebar">
<div id="wrapper">
    <!-- Top Navigation -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <ul class="nav navbar-top-links navbar-right">

            </ul>
            <!-- This is the message dropdown -->

            <ul class="nav navbar-top-links navbar-right pull-right">

                <!-- /.Task dropdown -->
                <ul class="dropdown pull-left m-t-10">
                    <li><a href="/" title="Перейти на сайт" target="_blank"><span class="circle circle-sm bg-success di"><i class="ti-home"></i></span></a></li>
                </ul>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="/images/user.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">ИМЯ</b><span class="caret"></span> </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-text"><h4>ИМЯ</h4><p class="text-muted">ПОЧТА</p></div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="ti-settings"></i> Настройки</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= route('logout')?>"><i class="fa fa-power-off"></i> Выйти</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- Left navbar-header -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Навигация</span></h3> </div>
            <div class="user-profile">
                <div class="dropdown user-pro-body"></div>
            </div>
            <ul class="nav" id="side-menu">
                <li> <a href="<?= route('admin.users.index') ?>" class="waves-effect"><i class="fa fa-user fa-fw"></i><span class="hide-menu">Настройки пользователей<span class="fa arrow"></span></span></a></li>
                <li class="">
                    <a href="#" class="waves-effect waves-light"><i class="fa fa-gears fa-fw"></i>
                        <span class="hide-menu">Настройка шаблона <span class="fa arrow"></span></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?= route('admin.template.index') ?>">
                                <span class="hide-menu">Шаблон</span></a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="waves-effect waves-light"><i class="fa fa-gears fa-fw"></i>
                        <span class="hide-menu">Прайсы <span class="fa arrow"></span></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?= route('admin.mainprice.index') ?>">
                                <span class="hide-menu">Главный</span></a>
                        </li>
                        <li><a href="<?= route('admin.additionprice.index') ?>">
                                <span class="hide-menu">Дополнительный</span></a>
                        </li>
                        <li><a href="<?= route('admin.pricegroup.index') ?>">
                                <span class="hide-menu">Категории</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"><?= $title ?></h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/admin/main">Главная</a></li>
                        <li class="active"><?= $title ?></li>
                    </ol>
                </div>
            </div>

            <?php include_once dirname(__FILE__) . '/../../layouts/' . $layout . '.php'; ?>

        </div>

        <footer class="footer text-center"> <?php echo date('Y') ?> </footer>
    </div>
</div>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script src="/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/js/jquery.inputmask.bundle.js"></script>
<script src="/js/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/waves.js"></script>
<script src="/admintheme/js/validator.js"></script>
<script src="/admintheme/js/dropify.min.js"></script>
<script src="/admintheme/js/bootstrap-datepicker.min.js"></script>
<script src="/admintheme/js/select2.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/admintheme/js/bootstrap-treeview.min.js"></script>
<script src="/admintheme/js/bootstrap-treeview-init.js"></script>
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script src="/admintheme/js/summernote-ru-RU.js"></script>
<script src="/admintheme/js/admin-custom.js"></script>



</body>
</html>
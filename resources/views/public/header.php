<!-- Main header -->
<header class="header">


    <div class="main-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-4 col-3">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="<?= route('web.main') ?>">
                            <img src="/images/logo.jpg" data-rjs="2" alt="НДП" style="width:70px;">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 d-none d-md-block">
                    <h4 style="margin-top: 8px;font-size: 1.2rem;color:#290F88">Новосибирский дом права</h4>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-5 col-sm-8 col-9">
                    <nav>
                        <!-- Header-menu -->
                        <div class="header-menu">
                            <ul>
                                <li><a href="<?= route('web.main') ?>">Главная</a></li>
                                <li><a href="<?= route('web.price') ?>">Прайс</a></li>
                                <li><a href="<?= route('web.contacts') ?>">Контакты</a></li>
                            </ul>
                        </div>
                        <!-- End of Header-menu -->
                    </nav>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-3 d-none d-md-block">
                    <!-- Header Call -->
                    <div class="header-call text-right">
                        <a href="tel:<?= $template->PhoneOneClear ?>"><?= $template->phone_one ?></a><br>
                        <a href="tel:<?= $template->PhoneTwoClear ?>"><?= $template->phone_two ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End of Main header -->
<!DOCTYPE html>
<html lang="Ru-ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $seo_title ?></title>
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <meta name="description" content="<?= $seo_description ?>"/>
    <meta name="keywords" content="<?= $seo_keywords ?>"/>
    <!-- open graph -->
    <meta property="og:url" content="<?= route('web.main') ?>"/>
    <meta property="twitter:widgets:csp" content="on"/>
    <meta property="og:type" content="article"/>
    <meta name="twitter:card" content="summary"/>
    <meta property="og:image" content="imageUrl"/>
    <meta property="og:title" content="<?= $seo_title ?>"/>
    <meta name="twitter:title" content="<?= $seo_title ?>"/>
    <meta name="title" content="<?= $seo_title ?>"/>
    <meta property="og:description" content="<?= $seo_description ?>"/>
    <meta name="description" content="<?= $seo_description ?>"/>
    <meta name="twitter:description" content="<?= $seo_description ?>"/>
    <meta name="keywords" content="<?= $seo_keywords ?>"/>
    <meta name="custom_text" content="<?= $seo_description ?>"/>
    <!-- /open graph -->
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">

    <!-- CSS Files -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500,700%7CSource+Sans+Pro:300i,400,400i,600,700">
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/plugins/swiper/swiper.min.css">
    <link rel="stylesheet" href="/web/plugins/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="/web/css/style.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/colors/theme-color-1.css">
    <link rel="stylesheet" href="/web/css/custom.css">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

</head>

<body>
<!-- Preloader -->
<div class="preLoader"></div>

<?php include_once dirname(__FILE__) . '/../header.php'; ?>
<?php include_once dirname(__FILE__) . '/' . $layout . '.php'; ?>
<?php include_once dirname(__FILE__) . '/../footer.php'; ?>


<!-- Back to top -->
<div class="back-to-top">
    <a href="#"> <i class="fa fa-chevron-up"></i></a>
</div>

<!-- JS Files -->
<script src="/web/js/jquery-3.3.1.min.js"></script>

<script src="/web/js/bootstrap.bundle.min.js"></script>
<script src="/web/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/web/plugins/waypoints/sticky.min.js"></script>
<script src="/web/plugins/swiper/swiper.min.js"></script>
<script src="/web/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/web/plugins/parsley/parsley.min.js"></script>
<script src="/web/plugins/retinajs/retina.min.js"></script>
<script src="/js/jquery.inputmask.bundle.js"></script>
<script src="/web/plugins/isotope/isotope.pkgd.min.js"></script>
<script src="/web/js/menu.min.js"></script>
<script src="/web/js/scripts.js"></script>
<script src="/web/js/custom.js"></script>

<script>
    $(document).ready(function () {
        $('.phone').inputmask({"mask": "+7 (999) 999-99-99"});
    });


</script>

</body>
</html>
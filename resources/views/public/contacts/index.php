<section class="page-title-wrap" data-bg-img="/web/img/bg-bread.jpg" data-rjs="2" style="background: url(/web/img/bg-bread.jpg) center center;">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title">
                    <h2><?= $title ?></h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="<?= route("web.main") ?>"><i class="fa fa-home"></i> Главная</a></li>
                        <li><a href="<?= route("web.contacts") ?>"><?= $title ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pt-60 pb-60">
    <div class="container">
        <div class="row">
            <div class="contact-map mb-10">

            </div>
            <div class="col-lg-12">
                <div class="contacts-main-map">
                    <div id="map" style="width: 100%; height: 400px"></div>
                </div>
                <script type="text/javascript">
                    ymaps.ready(init);
                    var myMap,
                        myPlacemark;

                    function init(){
                        myMap = new ymaps.Map("map", {
                            center: [55.042705, 82.985357],
                            zoom: 17.4,
                            behaviors: ['multiTouch']
                        });
                        myMap.behaviors.disable('scrollZoom');
                        <?php if ($template) { ?>
                        myPlacemark<?= $template->id ?> = new ymaps.Placemark([<?= $template->latitude ?>, <?= $template->landitude ?>], {


                        },{
                            // Опции.
                            // Необходимо указать данный тип макета.
                            iconLayout: 'default#image',
                            // Своё изображение иконки метки.
                            iconImageHref: '/images/marker.png',
                            // Размеры метки.
                            iconImageSize: [41, 60],
                            // Смещение левого верхнего угла иконки относительно
                            // её "ножки" (точки привязки).
                            iconImageOffset: [-5, -38],
                        });

                        myMap.geoObjects.add(myPlacemark<?= $template->id ?>);
                        <?php } ?>


                    }
                </script>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="page-contacts-widget-wrapper light-bg2 d-flex align-items-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <!-- Contact Info -->
                    <div class="page-contacts-widget">
                        <h3 class="h3"><?= $title ?></h3>
                        <div class="contact-widget-content"  style=" color: #000;">
                            <p style=" font-size: 18px;"><b><?= $template->company_name ?></b></p>
                            <ul class="list-unstyled" style=" font-size: 16px;">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="tel:<?= $template->PhoneOneClear ?>"><b><?= $template->phone_one ?></b></a>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="tel:<?= $template->PhoneTwoClear ?>"><b><?= $template->phone_two ?></b></a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <a href="mailto:<?= $template->mail ?>"><b><?= $template->mail ?></b></a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span><b><?= $template->address ?></b></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!-- Contact Form -->
                <div class="contact-form parsley-validate-wrap mt-60">
                    <h3 class="bordered-title">Написать нам</h3>
                    <div class="print-error-msg" style="display: none;">
                        <ul></ul>
                    </div>
                    <div class="form-response"></div>
                    <form  class="contact-form-style" id="sendFormcontacts"
                           action="<?= route('web.contacts.sendForm') ?>" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="text" name="name" class="theme-input-style" placeholder="Ваше имя" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="text" name="phone" class="theme-input-style phone" placeholder="Телефон" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-lg-12">
                                    <input type="hidden" name="subject" value="Письмо со страницы контакты">


                                </div>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea name="message" class="theme-input-style" placeholder="Введите ваш вопрос" required=""></textarea>
                        </div>
                        <button type="submit" class="btn">Отправить сообщение</button>
                        <?= csrf_field() ?>
                    </form>
                </div>
                <!-- End of Contact Form -->
            </div>
        </div>
    </div>
</section>

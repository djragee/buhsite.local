<section class="page-title-wrap" data-bg-img="/web/img/bg-bread.jpg" data-rjs="2" style="background: url(/web/img/bg-bread.jpg) center center;">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title">
                    <h2><?= $title ?></h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="<?= route("web.main") ?>"><i class="fa fa-home"></i> Главная</a></li>
                        <li><a href="<?= route("web.price") ?>"><?= $title ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pt-60 pb-30">
    <div class="container">
        <div class="row">
            <div class="col">
                <!-- Products Table -->
                <h1 class="text-center">Прайс-лист на бухгалтерские услуги</h1>
                <form class="parsley-validate" action="#">
                    <div class="primary-bg text-white p-2 mb-4 d-md-none">
                    </div>
                    <div class="table-responsive">
                        <table class="sn-cart light-bg mb-30 table table-bordered">
                            <tbody>
                            <tr>
                                <th>Услуга</th>
                                <th>ОСНО</th>
                                <th>УСН(доходы)</th>
                                <th>УСН(дох-расх)</th>
                                <th>ЕНВД</th>
                            </tr>
                            <?php foreach ($price_one as $price_item) { ?>
                            <tr>
                                <td>
                                    <strong><?= $price_item->name ?></strong><br>
                                    <small><?= $price_item->description ?></small>
                                </td>
                                <td>
                                    <span class="text-center"><?= $price_item->one_price ?> ₽</span>
                                </td>
                                <td>
                                    <span class="text-center"><?= $price_item->two_price ?> ₽</span>
                                </td>
                                <td>
                                    <span class="text-center"><?= $price_item->three_price ?> ₽</span>
                                </td>
                                <td>
                                    <span class="text-center"><?= $price_item->four_price ?> ₽</span>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <p>При увеличении численности работников доплачивается 1000 руб/мес за каждый 3 человек</p>
                    <p>*Действует при количестве первичной документации свыше 151 в месяц и количестве сотрудников 1-5 человек.</p>
                    <p>* Стандартный пакет бухгалтерских услуг включает в себя: ведение бухглатерского учета (введение банковских и кассовых операций, расчет з/п, учет расчетов, учет ТМЦ, ОС, НМА), введение
                    книги расходов и доходов, составление налоговой отчетности, сдача отчетности в ИФНС, ФСС, ПФ.</p>
                </form>
                <!-- End of Products Table -->
                <h1 class="text-center">Прайс-лист на дополнительные услуги</h1>
                <form class="parsley-validate" action="#">
                    <div class="primary-bg text-white p-2 mb-4 d-md-none">
                    </div>
                    <div class="table-responsive">
                        <table class="sn-cart light-bg mb-30 table table-bordered">
                            <tbody>
                            <tr>
                                <th>Услуга</th>
                                <th>Цена</th>
                            </tr>
                            <?php foreach ($price_group as $key => $value) { ?>
                                <tr><td colspan="2" class="text-center"><b><?= $value->name ?></b></td></tr>
                                <?php foreach ($value->getPrice as $item) {
                                    ?>
                                    <tr>
                                        <td>
                                            <strong><?= $item->name ?></strong><br>
                                        </td>
                                        <td>
                                            <span><?= $item->one_price ?> ₽</span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

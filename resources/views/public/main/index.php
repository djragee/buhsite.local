<!-- Abut Us -->
<section class="pt-30">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="section-title text-center">
                    <h1 style="color:#290F88">Новосибирский Дом Права</h1>
                    <h3><strong>Бухгалтерское обслуживание</strong> для малого и среднего бизнеса в Новосибирске</h3>
                </div>
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-md-4 col-sm-3">
                <div class="single-feature single-feature-img-top text-center" >
                    <div class="single-feature-img">
                        <img src="/web/img/icons/winner.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Достойно</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-3">
                <div class="single-feature single-feature-img-top text-center">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/reliability.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Надежно</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-3">
                <div class="single-feature single-feature-img-top text-center">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/money.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Выгодно</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div  style="background: #f3f3f3">
        <div class="container">
            <div class="row align-items-center pb-40" >
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="text-center">
                        <img src="/web/img/3.jpg" alt="" data-rjs="2" style="margin-top: 70px;" >
                    </div>
                    <div class="text-center light-bg2 mb-55 d-md-flex align-items-md-center justify-content-md-between">
                        <div class="col-md-12">
                            <h3 style="margin-top: 35px;">Бухгалтерский учет</h3>
                            <h4>ОТ 1500 РУБ/МЕС</h4>
                            <p style="padding: 16px;">*<small> На цену влияют: ведение складского учета и внешнеэкономическая деятельность</small></p>
                            <div class="col-md-12 text-center mb-30"><a href="<?= route('web.price') ?>" class="btn">Узнать цены на все услуги</a></div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="text-center">
                        <img src="/web/img/2.jpg" alt="" data-rjs="2" style="margin-top: 70px;" >
                    </div>
                    <div class="text-center light-bg2 mb-55 d-md-flex align-items-md-center justify-content-md-between">
                        <div class="col-md-12">
                            <h3 style="margin-top: 35px;">Регистрация ООО и ИП</h3>
                            <h4><strong><span  class="primary-bg text-white">БЕСПЛАТНО</span></strong></h4>
                            <p style="padding: 16px;">*<small> Подробности уточняйте по телефону <b><?= $template->phone_one ?></b> или <b><?= $template->phone_two ?></b></small></p>
                            <div class="col-md-12 text-center mb-30"><a href="<?= route('web.price') ?>" class="btn">Узнать цены на все услуги</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- End of About Us -->
<style>
    .service-details ul li {

        width: 100%;

    }
</style>
<section class="pt-60 pb-60">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="service-details animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h2>Для себя мы ставим задачи:</h2>
                    <ul class="rubik list-unstyled m-0 clearfix">
                        <li>Избавить вас от проблем с налогами и отчетностью</li>
                        <li>Решаем проблемы вашего предыдущего бухгалтера</li>
                        <li>Берем на себя общение с налоговыми органами</li>
                        <li>Предложить вам лояльную схему налогообложения Вашего бизнеса</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-img text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="animation-duration: 0.6s; animation-delay: 0.4s;">
                    <img src="/web/img/0.jpg" alt="" data-rjs="2">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Конец задач -->

<section class="pt-60 pb-60" style="background: #f3f3f3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
                <div class=" text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h2>Услуги предоставляемые нами</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Бухгалтерское сопровождение</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Электронная сдача отчетности</h4>

                </div>
            </div>

            <div class="col-lg-6 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Полный бухгалтерский учет <br>(ОСНО, ЕНВД, УСН, Патент)</h4>

                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Консультации профессионального бухгалтера</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Обслуживание ИП и ООО</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Нулевая отчетность</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Ведение кадрового учета</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Расчет заработной платы</h4>

                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Аудит</h4>

                </div>
            </div>


            <div class="col-lg-3 col-sm-6">
                <div class="single-service text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h4>Восстановление учета</h4>

                </div>
            </div>

        </div>
    </div>
</section>

<section class=" pt-50 pb-55">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-12 mb-50">
                <div class=" text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <h2>Почему выбирать нужно именно нашу компанию?</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".05" style="animation-duration: 0.6s; animation-delay: 0.05s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/one.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Уменьшение затрат на бухгалтерию</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="animation-duration: 0.6s; animation-delay: 0.2s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/two.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Работа напрямую с Главным бухгалтером</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".35" style="animation-duration: 0.6s; animation-delay: 0.35s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/three.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Гарантии, обсусловленные дороворными отношениями</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".5" style="animation-duration: 0.6s; animation-delay: 0.5s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/four.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Возможность контроллировать работу в режиме Online</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".65" style="animation-duration: 0.6s; animation-delay: 0.65s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/five.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Полная безопасность <br>данных</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".8" style="animation-duration: 0.6s; animation-delay: 0.8s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/six.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Обучение и экзаменация специалистов за свой счет</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="single-feature single-feature-img-left text-left animated fadeInUp" data-animate="fadeInUp" data-delay=".8" style="animation-duration: 0.6s; animation-delay: 0.8s;">
                    <div class="single-feature-img">
                        <img src="/web/img/icons/seven.svg" alt="" style="width: 50px">
                    </div>
                    <div class="single-feature-content">
                        <h4>Мы несем полную финансовую отвественность за свою работу</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
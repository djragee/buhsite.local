<!-- Footer -->
<footer class="main-footer">
    <div class="bottom-footer dark-bg">
        <div class="container">
            <div class="row align-items-center">
                <!-- Copyright -->
                <div class="col-md-6">
                    <div class="copyright-text text-center text-md-left">
                        <p class="mb-md-0"> <?= date('Y') ?>, <?= $template->company_name ?></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- End of Footer -->
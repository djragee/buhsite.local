<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() {
    Route::get('/','Admin\MainController@index', ['as' => 'admin']);
    Route::resource('template','Admin\TemplateController', ['as' => 'admin']);
    Route::resource('users','Admin\UserController', ['as' => 'admin']);
    Route::resource('mainprice', 'Admin\Price\MainPriceController', ['as'=>'admin']);
    Route::resource('mainprice_item', 'Admin\Price\MainPriceValueController', ['as'=>'admin']);
    Route::resource('additionprice', 'Admin\Price\AdditionPriceController', ['as'=>'admin']);
    Route::resource('pricegroup', 'Admin\Price\AdditionPriceGroupController', ['as'=>'admin']);
    Route::resource('additionprice_item', 'Admin\Price\AdditionPriceValueController', ['as'=>'admin']);
    Route::post('del_price_item','Admin\Price\MainPriceController@del_price_item', ['as' => 'admin'])->name('admin.mainprice.del_ajax');
});


Route::namespace('Web')->group(function () {
    Route::get('/', 'MainController@index')->name('web.main');
    Route::get('/contacts', 'ContactsController@index')->name('web.contacts');
    Route::post('/contacts/contactsSendForm', 'ContactsController@contactsSendForm')->name('web.contacts.sendForm');
    Route::get('/price', 'PriceController@index')->name('web.price');
    Route::get('/additionprice', 'AdditionPriceController@index')->name('web.additionprice');

});

Auth::routes();

Route::get('/register', function () {
    return redirect()->action('Web\MainController@index');
});
Route::get('/logout', 'Auth\LoginController@logout');
